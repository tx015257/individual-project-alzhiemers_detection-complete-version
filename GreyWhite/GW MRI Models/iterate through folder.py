# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 09:27:12 2020

@author: micha
"""

import os
import cv2
import numpy as np
import scipy.misc
import numpy.random as rng
from PIL import Image, ImageDraw, ImageFont
from sklearn.utils import shuffle
import nibabel as nib #reading MR images
import math
import glob
import matplotlib.pyplot as plt
import imutils


def get_perimeter(image):
    gray = cv2.medianBlur(image, 5, 0)
    gray=cv2.equalizeHist(gray,gray)
    
    # threshold the image, then perform a series of erosions +
    # dilations to remove any small regions of noise
    thresh = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=2)
    
    contours,hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        cnt = contours[0]
        perimeter = cv2.contourArea(cnt,True)
        return perimeter
    else:
        print ("No contour Found.")
        return 0
    
def find_brain(image, plot=False):
    # load the image, convert it to grayscale, and blur it slightly
    gray = cv2.GaussianBlur(image, (5, 5), 0)
    
    # threshold the image, then perform a series of erosions +
    # dilations to remove any small regions of noise
    thresh = cv2.threshold(gray, 35, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=2)
    
    # find contours in thresholded image, then grab the largest
    # one
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
    	cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    c = max(cnts, key=cv2.contourArea)
    
    # determine the most extreme points along the contour
    extLeft = tuple(c[c[:, :, 0].argmin()][0])
    extRight = tuple(c[c[:, :, 0].argmax()][0])
    extTop = tuple(c[c[:, :, 1].argmin()][0])
    extBot = tuple(c[c[:, :, 1].argmax()][0])
    
    # draw the outline of the object, then draw each of the
    # extreme points, where the left-most is red, right-most
    # is green, top-most is blue, and bottom-most is teal
    cv2.drawContours(image, [c], -1, (0, 255, 255), 2)
    cv2.circle(image, extLeft, 6, (0, 0, 255), -1)
    cv2.circle(image, extRight, 6, (0, 255, 0), -1)
    cv2.circle(image, extTop, 6, (255, 0, 0), -1)
    cv2.circle(image, extBot, 6, (255, 255, 0), -1)
    
    new_image = image[extTop[1]:extBot[1], extLeft[0]:extRight[0]]
    return new_image

outpath='/home/fs838120/Documents/Brain_MRI 2D/AD_Coronal_Slices'
path='/home/fs838120/Documents/Brain_MRI 2D/AD'

files = glob.glob(path)
print(len(files))

for image_path in os.listdir(path):
    input_path = os.path.join(path, image_path)
    epi_img = nib.load(input_path)
    epi_img_data = epi_img.get_fdata()
    epi_img_data.shape
    axial_slices = epi_img_data[:, :, 111:117]
    images = []
    perimeters = []
    for i in range(axial_slices.shape[2]):
        images.append((axial_slices[:,:,i]))

    total_slices= axial_slices.shape[2]

    images = np.asarray(np.uint8(images))
    print(len(images))
    for i in range(len(images)):
        perimeter=get_perimeter(images[i,:,:])
        perimeters.append(perimeter)
     
    maxslice=perimeters.index(max(perimeters))
    plt.imshow(images[maxslice,:,:],cmap='gray')
    plt.axis('off')
    image_path_wo,ext=os.path.splitext(image_path)
    fullpath = os.path.join(outpath, 'sliced_'+image_path_wo+'.png')
    cv2.imwrite(fullpath, images[maxslice,:,:].T)

new_files = glob.glob(outpath)
print(len(new_files))